/* Janae Gates
   janaeg
   Lab 5
   Section 001
   Nushrat Humaira
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  int a, b, d;
  bool e;
  Suit c;
  Card temp;
  b = 2;
  d = 0;
  c = SPADES;
  Card deck[52];

  for(a = 0; a < 52; a++)
  {
     deck[a].value = b;
     deck[a].suit = c;
     if(b == 14)
     {
	b = b - 12;
	if(d == 0)
	   c = HEARTS;
	else if(d == 1)
	   c = DIAMONDS;
	else if(d == 2)
	   c = CLUBS;
	d++;
     }
     else
	b++;
  }
  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  Card * start;
  Card * end;
  start = &deck[0];
  end = &deck[51];
  random_shuffle(start, end, myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card arr[5];
  for (a = 0; a < 5; a++)
     arr[a] = deck[a];

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
  for(a = 0; a < 4; a++)
     for(b = a + 1; b < 5; b++)
     {
	e = suit_order(arr[a],arr[b]);
	if(e == false)
	{
	   temp = arr[a];
	   arr[a] = arr[b];
	   arr[b] = temp;
	}
     }

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
  for(b = 0; b < 5; b++)
  {
     std::cout << std::right << std::setw(10) << get_card_name(arr[b]);
     printf(" of ");
     std::cout << get_suit_code(arr[b]);
     printf("\n");
  }


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  bool a;
  a = true;
  if(lhs.suit == rhs.suit)
     if(lhs.value > rhs.value)
	a = false;
  if(lhs.suit > rhs.suit)
     a = false;
  return a;

}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  switch (c.value)
  {
     case 2:	return "2";
     case 3:	return "3";
     case 4:	return "4";
     case 5: 	return "5";
     case 6: 	return "6";
     case 7: 	return "7";
     case 8:	return "8";
     case 9: 	return "9";
     case 10:	return "10";
     case 11: 	return "Jack";
     case 12: 	return "Queen";
     case 13:	return "King";
     case 14:	return "Ace";
     default:	return "";
  }
     
}

